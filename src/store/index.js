import axios from 'axios'

import { createStore } from 'vuex'

export default createStore({
  state: {  
    cart: {
      items: [],
    },
    books: [],
    bookClubs:[],
    kidsItems:[],
    flights: [],
    tours:[],
    orders: [],
    cartItemCount: 0,
    watchingItems: [],
    watchListCount: 0,
    isAuthenticated: false,
    token: '',
    isLoading: false,
  },
  
  mutations: {
    
    INITIALIZE_STORE(state){

      if(localStorage.getItem('cart')){
        state.cart = JSON.parse(localStorage.getItem('cart'));
      } else {
        localStorage.setItem('cart', JSON.stringify(state.cart));
      }

      if (localStorage.getItem('token')) {
        state.token = localStorage.getItem('token');
        state.isAuthenticated = true;
      } else {
          state.token = '';
          state.isAuthenticated = false;
      }
    },

    SET_IS_LOADING(state, status){
      state.isLoading = status;
    },

    SET_IS_AUTHENTICATED(state, status) {
      state.isAuthenticated = status;
    },

    ADD_TO_CART(state, item){
      this.cartItemCount++;
      const exist = state.cart.items.filter(i => i.id === item.id);

      if(exist.length){
        exist[0].quantity = parseInt(exist[0].quantity) + parseInt(item.quantity);
      } else {
        state.cart.items.push(item);
      }
   
    },

    REMOVE_FROM_CART(state, item) {
      state.cart.items = state.cart.items.filter(i => i.id !== item.id);
    },

    UPDATE_CART(state) {
      localStorage.setItem('cart', JSON.stringify(state.cart));
    },

    SET_TOKEN(state, token) {
      state.token = token;
      state.isAuthenticated = true;
    },
      
    REMOVE_TOKEN(state) {
      state.token = '';
      state.isAuthenticated = false;
    },

    SET_CATEGORIES(state, books) {
      state.books = books;
    },

    SET_BOOKS(state, books) {
      state.books = books;
    },

    SET_BOOK_CLUBS(state, bookClubs) {
      state.bookClubs = bookClubs;
    },

    SET_FLIGHTS(state, flights) {
      state.flights = flights;
    },

    SET_TOURS(state, tours){
      state.tours = tours;
    },
    
    ADD_TO_WATCHLIST(state, item) {
      this.watchListCount++;
      
      if (item) {
        state.watchingItems.splice(
          this.watchingItems.findIndex(
            (p) => p.id === item.id
          ),
          1
        );
     
      } else {
        state.watchingItems.push(item);
      }

    },

    REMOVE_FROM_WATCHLIST(state, item) {
      state.watchingItems.splice(
        state.watchingItems.findIndex((p) => p.id === item.id),
        1
      );

    },

    UPDATE_WATCHLIST(){
      localStorage.setItem('watchingItems', JSON.stringify(state.watchingItems));
    }

  },

  actions: {

    setIsLoading({commit}, status){
      commit('SET_IS_LOADING', status);
    },

    setIsAuthenticated({commit}, status){
      commit('SET_IS_AUTHENTICATED', status);
    },

    addToCart({commit}, item){
      commit('ADD_TO_CART', item);
      commit('UPDATE_CART');
    },
    
    removeFromCart({commit}, item){
      commit('REMOVE_FROM_CART', item);
      commit('UPDATE_CART');
    },

    setToken({commit}, token){
      commit('SET_TOKEN', token);
    },

    removeToken({commit}, token){
      commit('REMOVE_TOKEN', token);
    },

    setCategories({commit}, books){
      commit('SET_CATEGORIES', books);
    },

    addToWatchList({commit}, item){
      commit('ADD_TO_WATCHLIST', item);
      commit('UPDATE_WATCHLIST');
    },

    removeFromWatchList({commit}, item){
      commit('REMOVE_FROM_WATCHLIST', item);
      commit('UPDATE_WATCHLIST');
    },

    async fetchCategory({commit}, categorySlug) {
      
      commit('SET_IS_LOADING', true);

      try {
        const response = await axios.get(`/api/v1/books/${categorySlug}/`);
        commit("SET_CATEGORIES", response.data);
      } catch(error) {
        console.log(error);
      }
      
      commit('SET_IS_LOADING', false);
    },

    async getAllOrders({commit}) {
      commit('SET_IS_LOADING', true)

      try {
        const response = await axios.get('/api/v1/orders/');
        commit('SET_BOOKS', response.data);
        commit('SET_KIDS_ITEMS', response.data);
        commit('SET_FLIGHTS', response.data);
        commit('SET_TOURS', response.data);

      } catch(error){
        console.log(error)
      }

      commit('SET_IS_LOADING', false)
    },

    async getLatestBooks({commit}){
      commit('SET_IS_LOADING', true);

      try {
        const response = await axios.get('api/v1/books/latest-books/')
        commit('SET_BOOKS', response.data);
      } catch (error) {
        console.log(error)
      }
      commit('SET_IS_LOADING', false)
    },

    async getLatestBookClubs({commit}){
      commit('SET_IS_LOADING', true)

      try {
        const response = await axios.get('api/v1/flights/latest-flights/')
        commit('SET_KIDS_ITEMS', response.data);
      } catch (error) {
        console.log(error)
      }
      commit('SET_IS_LOADING', false)
    },

    async getLatestFlights({commit}){
      commit('SET_IS_LOADING', true)
      try {
        const response = await axios.get('api/v1/flights/latest-flights/')
        commit('SET_FLIGHTS', response.data);
      } catch (error) {
        console.log(error)
      }
      commit('SET_IS_LOADING', false)
    },

    async getLatestTours({commit}){
      commit('SET_IS_LOADING', true);

      try {
        const response = await axios.get('api/v1/flights/latest-flights/')
        commit('SET_TOURS', response.data);
      } catch (error) {
        console.log(error)
      }

      commit('SET_IS_LOADING', false)
    },
    
},

  getters: {

    getCart: state => {
      return state.cart;
    },

    getCartItems: state => {
      return state.cart.items;
    },

    cartTotalPrice: state => {
      let total = 0.0;
      state.cart.items.forEach( (record) => {
        total += record.price * record.quantity;
      })
      return total;
    },

    getCategories: state => {
      return state.books;
    },

    getWatchingItems: state => {
      return state.watchingItems;
    },

    getMyOrdersList: state => {
      return state.orders;
    },

    getBooks: state => {
      return state.books;
    },

    getKidsItems: state => {
      return state.kidsItems;
    },

    getFlights: state => {
      return state.flights;
    },

    getTours: state => {
      return state.tours;
    },

    getBookByTitle(state){
      console.log(state.books)
      
      const query = state.books.filter( book => {
        console.log(book.title),
        book.title == 'TEST'; 
      })
      console.log(query)
      return query
    },

    getBookByID(state, bookID) {
      return state.watchingItems.find(
        (p) => p.id === bookID
      );
    },

    getKidsItemByName(state){
      console.log(state.kidsItems)
      
      const query = state.kidsItems.filter( kidItem => {
        console.log(kidItem.name),
        kidItem.name == 'TEST'; 
      })
      console.log(query)
      return query
    },

    getKidsItemByID(state, kidItemID) {
      return state.watchingItems.find(
        (p) => p.id === kidItemID
      );
    },

    getFlightByName(state){
      console.log(state.flights);
      
      const query = state.flights.filter( flight => {
        console.log(flight.name),
        flight.name == 'TEST'; 
      })
      console.log(query)
      return query
    },

    getFlightByID(state, flightID) {
      return state.watchingItems.find(
        (p) => p.id === flightID
      );
    },

    getTourByName(state){
      console.log(state.tours);
      
      const query = state.tours.filter( tour => {
        console.log(tour.name),
        tour.name == 'TEST'; 
      })
      console.log(query)
      return query
    },

    getTourByID(state, tourID) {
      return state.watchingItems.find(
        (p) => p.id === tourID
      );
    },

    getToken: state => {
      return state.token;
    },

    getIsLoading: state => {
      return state.isLoading;
    },

  },

  modules: {
  }
})
